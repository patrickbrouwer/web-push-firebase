# web push notification with Firebase Cloud messaging

how to use:
- go to [https://console.firebase.google.com](https://console.firebase.google.com)
- make a project (US server, EU not supported for web)
- click to make a web and copy the variables in web/webpush.html
- copy the senderId and add it to web/firebase-messaging-sw.js

it only works on webserver that are hosted on localhost or elsewhere

## get token from browser
- open browser on web/webpush.html
- copy token
- past token in send/firebase-node-send.js as string in registration_ids array

## send message
- go to [https://console.firebase.google.com](https://console.firebase.google.com) 
- open project setting 
- tab cloud messaging
- copy server key
- open send/firebase-node-send.js
- paste server key in 'serverkey' variable
- edit message if you want
- open console 
  - ```npm install``` (to install request)
  - run ```node firebase-node-send.js```
    - message wil be send