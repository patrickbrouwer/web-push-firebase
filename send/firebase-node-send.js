var request = require("request");

var payload = {
	"data": { //data
	  "message": {
		"notification": {
		  "title": "this is a notificaton title", //notification title
		  "body": "this is a notfiction body", //notification text body
		  "icon" : "/push_icon.png", //notification icon
		},
	  } 
	},
//registration ids from the browser after registration 
  "registration_ids" : [
	"*****",
	"*****1"
	]
}
//key (Serversleutel from project settings, cloud messaging *https://console.firebase.google.com*)
var serverkey = '*****'


var options = { method: 'POST',
  url: 'https://fcm.googleapis.com/fcm/send',
  headers: 
   {
		'cache-control': 'no-cache',
		'Content-Type': 'application/json',
		
		Authorization: 'key='+serverkey 
	},
	body: JSON.stringify(payload)
}
request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});
