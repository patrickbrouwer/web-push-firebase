// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.8.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in the
// @TODO messagingSenderId.
firebase.initializeApp({
  'messagingSenderId': '******'
});

/*
Retrieve an instance of Firebase Messaging so that it can handle background messages.
*/
const messaging = firebase.messaging()
messaging.setBackgroundMessageHandler(function(payload) {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  
  //parce payload
  notificationData = JSON.parse(payload.data.message);
  
  // Customize notification here
  const notificationTitle = notificationData.notification.title;
  const notificationOptions = {
    body: notificationData.notification.body,
    icon: notificationData.notification.icon
  };

  //show notifiction
  return self.registration.showNotification(notificationTitle,
      notificationOptions);
});